use std::collections::HashMap;
use std::fmt;
use std::error;

use geo::Point;
use json::JsonValue;

use crate::osm::StreetNode;

#[derive(Debug)]
pub enum MalformedDataError {
    DataIsNotObject,
    NoElementsKey,
    ElementsKeyNotArray,
    ElementWithNoTypeKey{ index: usize },
    ElementWithNoIdKey{ index: usize },
    NodeWithInvalidLon{ index: usize },
    NodeWithInvalidLat{ index: usize },
    UnknownElementType{ index: usize, type_name: String },
}

impl fmt::Display for MalformedDataError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl error::Error for MalformedDataError {}

pub struct Storage {
    ways: HashMap<u64, Vec<u64>>,
    nodes: HashMap<u64, Point<f64>>,
}

impl Storage {
    pub fn new() -> Storage {
        Storage {
            ways: HashMap::new(),
            nodes: HashMap::new(),
        }
    }

    pub fn load_json(&mut self, data: JsonValue) -> Result<(usize, usize), MalformedDataError> {
        let inserted_nodes = 0;
        let inserted_ways = 0;

        if !data.is_object() {
            return Err(MalformedDataError::DataIsNotObject);
        }

        if !data.has_key("elements") {
            return Err(MalformedDataError::NoElementsKey);
        }

        if !data["elements"].is_array() {
            return Err(MalformedDataError::ElementsKeyNotArray);
        }

        for (index, element) in data["elements"].members().enumerate() {
            let typename = if let Some(s) = element["type"].as_str() {
                s
            } else {
                return Err(MalformedDataError::ElementWithNoTypeKey{index});
            };

            let id = if let Some(x) = element["id"].as_u64() {
                x
            } else {
                return Err(MalformedDataError::ElementWithNoIdKey{index});
            };

            if typename == "way" {
                self.ways.insert(
                    id,
                    element["nodes"]
                        .members()
                        .map(|n| n.as_u64())
                        .filter(|o| o.is_some())
                        .map(|o| o.unwrap())
                        .collect(),
                );
            } else if typename == "node" {
                self.nodes.insert(
                    id,
                    Point::new(
                        if let Some(lon) = element["lon"].as_f64() {
                            lon
                        } else {
                            return Err(MalformedDataError::NodeWithInvalidLon{index});
                        },
                        if let Some(lat) = element["lat"].as_f64() {
                            lat
                        } else {
                            return Err(MalformedDataError::NodeWithInvalidLat{index});
                        },
                    ),
                );
            } else {
                return Err(MalformedDataError::UnknownElementType{index, type_name: typename.to_string()});
            }
        }

        Ok((inserted_ways, inserted_nodes))
    }

    pub fn streets_from_gps(&self, gps: Point<f64>) -> impl Iterator<Item=StreetNode> {
        vec![StreetNode::dummy()].into_iter()
    }

    pub fn nodes(&self) -> usize {
        self.nodes.len()
    }

    pub fn ways(&self) -> usize {
        self.ways.len()
    }
}

#[cfg(test)]
mod tests {
    use json::object;
    use super::Storage;

    #[test]
    fn test_load() {
        let json_data = json::parse(r#"{
            "version": 0.6,
            "generator": "Overpass API 0.7.56.1002 b121d216",
            "osm3s": {
                "timestamp_osm_base": "2020-03-13T05:54:02Z",
                "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL."
            },
            "elements": [
                {
                    "type": "way",
                    "id": 29651042,
                    "nodes": [
                        1606050381,
                        1134835407,
                        1606050365,
                        2524614824,
                        1606050338,
                        1134836314,
                        1582281930,
                        1582281918
                    ],
                    "tags": {
                        "highway": "secondary",
                        "lanes": "4",
                        "name": "20 de noviembre",
                        "oneway": "no"
                    }
                }
            ]
        }"#).unwrap();

        let mut storage = Storage::new();

        storage.load_json(json_data).unwrap();
    }
}
