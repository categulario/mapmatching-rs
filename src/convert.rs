use geo::{LineString, Coordinate};
use json::JsonValue;

pub fn json_to_line_string(json: JsonValue) -> Result<LineString<f64>, ()> {
    Ok(json["features"][0]["geometry"]["coordinates"].members().map(|v| Coordinate {
        x: v[0].as_f64().unwrap(),
        y: v[0].as_f64().unwrap(),
    }).collect())
}
