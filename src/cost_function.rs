use geo::Point;
use geo::prelude::*;

use crate::osm::StreetNode;
use crate::algorithms::a_star;

/// Trait that describes the cost of two gps points and two associated streets
pub trait CostFunction: Send {
    /// Get the cost for the given gps positions and their associated streets
    fn compute(&self, point1: Point<f64>, street1: &StreetNode, point2: Point<f64>, street2: &StreetNode) -> (f64, Vec<StreetNode>);
}

/// This cost function is an arbitrary interpretation of the paper where this
/// algorithm is from.
pub struct NaiveCostFunction;

impl CostFunction for NaiveCostFunction {
    fn compute(&self, point1: Point<f64>, street1: &StreetNode, point2: Point<f64>, street2: &StreetNode) -> (f64, Vec<StreetNode>) {
        let (length, path) = a_star(street1, street2);

        // difference between path length and great circle distance
        // between the two gps points
        let mut curcost = (length - point1.haversine_distance(&point2)).abs().log10();

        // distance between start of path and first gps point
        curcost += street1.position().haversine_distance(&point1);

        // distance between end of path and second gps point
        curcost += street2.position().haversine_distance(&point2);

        (curcost, path)
    }
}

#[cfg(test)]
mod tests {
    use super::{CostFunction, NaiveCostFunction};
    use crate::osm::StreetNode;

    #[test]
    fn test_naive_cost() {
        let cf = NaiveCostFunction;

        let (cost, path) = cf.compute(
            (0.0, 0.0).into(),
            &StreetNode::new((0.0, 0.0).into()),
            (0.0, 0.0).into(),
            &StreetNode::new((0.0, 0.0).into()),
        );

        assert_eq!(cost, 59.9);
    }
}
