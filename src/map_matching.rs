use std::rc::Rc;
use std::collections::VecDeque;
use std::cmp::min;

use geo::Point;

use crate::storage::Storage;
use crate::osm::StreetNode;
use crate::response::MapMatchingResponse;
use crate::request::MapMatchingRequest;
use crate::error::MapMatchingError;

/// This node is used internally to build a DAG in which each node represents
/// a road that is near a gps position.
#[derive(Debug)]
struct Node {
    gps: Point<f64>,
    street: StreetNode,
    parent: Option<Rc<Node>>,
    cost: f64,
    path: Vec<StreetNode>,
}

impl Node {
    fn new(gps: Point<f64>, street: StreetNode, parent: Option<Rc<Node>>, cost: f64, path: Vec<StreetNode>) -> Node {
        Node {
            gps,
            street,
            parent,
            cost,
            path,
        }
    }

    /// Creates a dummy node to be used as initial value when reducing an
    /// iterator of nodes in search for the one with the minimum cost.
    fn dummy() -> Node {
        Node {
            gps: (0.0, 0.0).into(),
            cost: std::f64::INFINITY,
            parent: None,
            street: StreetNode::dummy(),
            path: Vec::new(),
        }
    }

    /// Creates a node of zero cost only for the first layer of the algorithm.
    fn for_first_layer(gps: Point<f64>, street: StreetNode) -> Node {
        Node {
            gps,
            street,
            cost: 0.0,
            parent: None,
            path: Vec::new(),
        }
    }

    /// Consumes this node and returns the important parts: the street and 
    /// its parent.
    fn into_pieces(self) -> (StreetNode, Option<Rc<Node>>) {
        (self.street, self.parent)
    }
}

/// Process a request to match the given GPS track to the streets stored in the
/// storage.
pub fn map_match(request: MapMatchingRequest, storage: &Storage) -> Result<MapMatchingResponse, MapMatchingError> {
    if request.path.num_coords() < 2 {
        return Err(MapMatchingError::NotEnoughNodes);
    }

    let mut positions = request.path.points_iter().take(request.node_count.count());

    // in the initial layer there are no parent nodes and the cost for all
    // nodes is zero
    let mut last_layer = Vec::new();
    let first_point = positions.next().unwrap();

    for street in storage.streets_from_gps(first_point) {
        last_layer.push(Rc::new(Node::for_first_layer(first_point, street)));
    }

    for pos in positions {
        // a temporary vector holds the nodes of the current layer
        let mut next_layer = Vec::new();

        for street in storage.streets_from_gps(pos) {
            // here what we do is fin the node in the previous layer with the
            // lowest cost to get to this node
            let (cost, parent, path) = last_layer.iter().map(|prev_node| {
                let (mut cost, path) = request.cost_function.compute(prev_node.gps, &prev_node.street, pos, &street);

                cost += prev_node.cost;

                (cost, prev_node.clone(), path)
            }).fold((std::f64::INFINITY, Rc::new(Node::dummy()), Vec::new()), |acc, (cost, node, path)| {
                if cost < acc.0 {
                    (cost, node, path)
                } else {
                    acc
                }
            });

            next_layer.push(Rc::new(Node::new(
                pos,
                street,
                Some(parent),
                cost,
                path,
            )));
        }

        // Replace the contents of last_layer with the newly constructed one
        last_layer.clear();
        last_layer.append(&mut next_layer);
    }

    // From the last layer choose the node with the lowest cost
    let (cost, mut path_head) = last_layer
        .into_iter()
        .map(|node| (node.cost, node))
        .fold((std::f64::INFINITY, Rc::new(Node::dummy())), |acc, (cost, node)| {
            if cost < acc.0 {
                (cost, node)
            } else {
                acc
            }
        });

    // a deque to collect all the nodes that make the final path
    let mut final_path = VecDeque::with_capacity(
        min(request.path.num_coords(), request.node_count.count())
    );

    loop {
        let (street, parent) = Rc::try_unwrap(path_head).unwrap().into_pieces();

        final_path.push_front(street);

        path_head = match parent {
            Some(node) => node.clone(),
            None => break,
        }
    }

    Ok(MapMatchingResponse {
        cost,
        path: final_path.into(),
    })
}
