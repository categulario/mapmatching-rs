use geo::Point;

/// Represents a node from the street graph
#[derive(Debug)]
pub struct StreetNode {
    position: Point<f64>,
}

impl StreetNode {
    pub fn new(position: Point<f64>) -> StreetNode {
        StreetNode {
            position,
        }
    }

    pub fn dummy() -> StreetNode {
        StreetNode {
            position: (0.0, 0.0).into(),
        }
    }

    pub fn position(&self) -> Point<f64> {
        self.position
    }
}
