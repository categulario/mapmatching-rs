use std::fs::File;
use std::io::Read;

use json;
use clap::{App, Arg, SubCommand, ArgMatches};

mod map_matching;
mod storage;
mod cost_function;
mod request;
mod response;
mod osm;
mod error;
mod convert;
mod algorithms;

use map_matching::map_match;
use storage::Storage;
use convert::json_to_line_string;
use request::MapMatchingRequest;
use cost_function::NaiveCostFunction;

fn simple_match(storage: Storage, matches: &ArgMatches) -> std::io::Result<()> {
    for file in matches.values_of("INPUT").unwrap() {
        let mut gps_file = File::open(file)?;
        let mut gps_contents = String::new();

        gps_file.read_to_string(&mut gps_contents)?;

        let gps_json = json::parse(&gps_contents).unwrap();
        let gps = json_to_line_string(gps_json).unwrap();
        let request = MapMatchingRequest {
            path: gps,
            radius: matches.value_of("radius").unwrap().parse().unwrap(),
            node_count: matches.value_of("node_count").unwrap().parse().unwrap(),
            cost_function: Box::new(NaiveCostFunction),
        };
        let response = map_match(request, &storage);

        match response {
            Ok(answer) => println!("{}", answer),
            Err(error) => eprintln!("{}", error),
        }
    }

    Ok(())
}

fn server(storage: Storage, matches: &ArgMatches) -> std::io::Result<()> {
    Ok(())
}

fn main() -> std::io::Result<()> {
    let matches = App::new("mapmatching")
        .version("1.0")
        .about("Matches a gps track (or a set of) against the street graph")
        .author("Abraham Toriz")
        .arg(
            Arg::with_name("data")
            .help("The street data from OSM")
            .short("d")
            .long("data")
            .takes_value(true)
            .required(true)
        )
        .subcommand(
            SubCommand::with_name("match")
            .about("Matches the given input files agains the loaded street data")
            .arg(
                Arg::with_name("INPUT")
                .help("The gps input file[s]")
                .multiple(true)
                .required(true)
            )
            .arg(
                Arg::with_name("radius")
                .help("The radius in meters for searching streets around a gps point")
                .long("radius")
                .short("r")
                .takes_value(true)
                .default_value("50")
            )
            .arg(
                Arg::with_name("node_count")
                .help("Limit the match algorithm to the first N gps position")
                .long("nodes")
                .short("n")
                .takes_value(true)
                .default_value("all")
            )
        )
        .subcommand(
            SubCommand::with_name("server")
            .about("Starts an http server that will respond mapmatching queries")
        )
        .get_matches();

    let datafilename = matches.value_of("data").unwrap();
    let mut datafile = File::open(datafilename)?;
    let mut contents = String::new();

    datafile.read_to_string(&mut contents)?;

    let data = json::parse(&contents).unwrap();
    let mut storage = Storage::new();

    storage.load_json(data).unwrap();

    if let Some(matches) = matches.subcommand_matches("match") {
        return simple_match(storage, matches);
    } else if let Some(matches) = matches.subcommand_matches("server") {
        return server(storage, matches);
    }

    Ok(())
}
