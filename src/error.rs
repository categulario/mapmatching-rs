use std::fmt;

/// An error that can occur during the map matching algorithm
#[derive(Debug)]
pub enum MapMatchingError {
    /// To start matching at least two gps positions are needed
    NotEnoughNodes,
}

impl fmt::Display for MapMatchingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
