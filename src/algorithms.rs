use crate::osm::StreetNode;

pub fn a_star(origin: &StreetNode, destination: &StreetNode) -> (f64, Vec<StreetNode>) {
    unimplemented!();
}

#[cfg(test)]
mod tests {
    use super::a_star;

    #[test]
    fn test_a_star() {
        assert!(false, "Can find a short path");
    }
}
